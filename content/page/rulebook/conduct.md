---
title: "Code of Conduct"
draft: false
date: 2022-02-04T15:31:30-05:00
menu:
  sidebar:
    parent: Rulebooks
slug: rulebooks/conduct
weight: 15
autonumbering: true
---

## Community/League Code of Conduct

1. Players are expected to be respectful of themselves, others, and Staff both in voice chat, text channels, and any social media interactions relating to Frontier Doubles League (Ex. Twitch, Twitter, Discord, etc.).
    * Rules may be changed, added, and removed by Frontier Doubles League as needed.
2. Behaviors deemed by the Frontier Doubles League to be toxic, inappropriate, unsporting, or illegal are not allowed in Frontier Doubles League. Examples of these can include, but are not limited to:
    * Slurs, Racism, Homophobia, Hate Speech, NSFW Content, Transphobia, etc.
    * Insulting or demeaning comments with malicious intent towards any other community member.
    * Disruptive behavior in official matches and scrims within the Frontier Doubles League Community as determined by the Moderation team.
    * Revealing personal and/or confidential information, without the written consent of the player (Doxxing).
    * Abusing text and voice chats within the Frontier Doubles League server which can include, but is not limited to: Spamming, Excessive Pings, Excessive Volume/Loudness,  Toxicity, Threats, etc.
    * Threats of violence for any reason.
    * Images/Videos involving Alcohol or Drugs.
    * Verbal or Text harassment towards other community members, including Staff.
        * Banter is allowed in Frontier Doubles League, however the line between banter and toxicity can be thin and will be handled on a case-by-case basis.
    * Scamming/Phishing.
    * Consistent number of reports of rule-breaking behavior within Frontier Doubles League.
3. Any instances of a community member breaking one of these rules is subject to the evaluations of the Moderation team on a case-by-case basis.
4. Punishments for violations of these rules are referred to as ‘Strikes’ and will be as follows:
    * 1st Strike: One (1) week suspension from all official league matches and community events along with a 1 week mute.
    * 2nd Strike: Suspension from all official league matches and community events along with a mute for the remainder of the season.
    * 3rd Strike: Permanent Ban from Frontier Doubles League.
        * Illegal activity of any kind will receive an immediate ban.
    * Players may be given more than one strike at a time based on the number of infractions.
5. Members who have received strikes may appeal these strikes through this form: https://forms.gle/Qvqv4kRqBfqFyQSk6
    * Submitting an appeal form will not guarantee that strikes will be removed.
    * Members who submit an appeal form will have their form inspected and issues taken to the Moderation team and Congress if needed to be discussed thoroughly.
    * It may take up to seven (7) business days for the appeal form to be accepted or declined and for strikes to be rescinded.
    * Members who have received two strikes for different offenses may submit a form for each strike.
        * Ex. If a player has received a strike for harassment and a strike for spamming, they may submit a form for each offense. If a player has received two (2) strikes for one (1) offense they may only submit one (1) form.
    * Bans may not be appealed.
    * Members who have had their appeal forms declined will not be allowed to submit an appeal form for the same issue; appeal form decisions are final.
    * Players must be in the FDL Discord to receive a response to their appeal form submission.
6. Self-promotion via outside the #self-promotion text channel including advertising Twitch channels, YouTube channels, organizations, or businesses on Discord, is not allowed.
7. Frontier Doubles League is an “at will” organization. Players or members of the community may be warned, muted, suspended, or permanently banned at the discretion of the Frontier Doubles League. Members must be given a reason by Frontier Doubles League Moderation Staff and/or Congress.
8. Mutes, suspensions, and warnings will remain private between the community member and staff involved. 
9. Bans may be made public.
10. Frontier Doubles League may take into account actions outside of Frontier Doubles League in extreme cases.
    * Frontier Doubles League Twitch chat is considered ‘inside’ Frontier Doubles League.
11. Offenses will be reset at the start of a new season unless the player in question has received two (2) or more offenses, in which case it will be up to the current Frontier Doubles League Congress and Moderation team on whether to reset their offenses or not.
12. Media posted that is deemed to have sensitive content or breaks any rules previously listed is not permitted.
13. NSFW Discord profile pictures and names are not allowed in FDL and you may be asked to change if needed. Refusal may result in a strike.
14. Abuse of the FDL Mailbox may result in a strike. Please keep all messages to the FDL Mailbox appropriate and with intent to seek help from staff.

### Match/League Conduct
1. Matches may be streamed from the point of view of the players, except when streamed by the Frontier Doubles League official channels.
2. Matches for the week must be scheduled by Wednesday at 11:59pm EST and played by Sunday at 11:59pm EST, unless extenuating circumstances occur. If extenuating circumstances occur, Captains and/or General Managers must contact a member of League Operations.
    * Failure to schedule and/or play on time may result in punishment to one or more members of the team(s) in question.
    * Leaving a match early (before the scoreboard has been shown at the end of gameplay) may result in punishment from League Operations and/or Moderation. 
3. Failure to save and submit replays may result in punishment along with a potential forfeiture of the match. 
4. If a player belonging to a team leaves during the season they will receive a suspension for the remainder of the season and not be allowed to come back and be signed until the next off-season begins.
5. Unsportsmanlike conduct will not be tolerated on the field within the in-game chat and strikes may be given to players in question if comments are deemed inappropriate.
