#!/usr/bin/python
"""generator.py

Usage:
    generator.py [--rules=FILE] [--output=FILE] [--debug]
    generator.py -h | --help

Takes a given league config file that contains the match information and assign 

Options:
    -h --help      Show this information
    -d --debug     Show debug output.
    --rules=FILE   Rules file to generate html off of. [default: rules.json]
    --output=FILE  Output file to write to. [default: index.html]
"""

import os
import sys
import json
import logging
from docopt import docopt


def main(args):
    """
    Main function.

    Format for the 

    """
    try:
        file = args['--rules']
        # attempt to open the file
        logging.debug(f"Opening League file {file}")
        with open(file) as f:
            data = json.load(f)
        logging.debug(f"Config file opened, contents: {data}")
    except Exception as e:
        logging.error(f"Error opening {file}: {e}")
        sys.exit(1)

    version = '3.0.0'
    html = f"""
<html>
    <head>
        <title>Frontier Doubles League Rules and Regulations | v{version}</title>
        <style>
            ol {{
                list-style: none;
                counter-reset: section;
                margin: 5px;
            }}
            li {{
                line-height: 1.5;
            }}
            ol li {{ 
                font-weight: 700 
            }}
            ol li ol li {{
                font-weight: 300
            }}
            ol li::before {{
                counter-increment: section;
                content: counters(section, ".") " ";
            }}
            ol li {{
                margin: 0px;
                padding: 0px;
                text-indent: -1.5em;
                margin-left: 1em;
            }}
            ol li ol li::before {{
                counter-increment: section;
                content: counters(section, ".") " ";
            }}
        </style>
    </head>
    <body>
    <div style="font-size: 300%; font-weight: bold;" align="center">{data['name']}</div>
    <div style="font-size: 150%" align="center">{data['preface']}</div>
    <ol id="root">
        {parse_output(data)}
    </ol>
    </body>
</html>
"""
    print(html)

def parse_output(data):
    """
    Parse output into a list.
    This can be recursively called.
    """
    output = ""
    for entry in data['data']:
        output += f"<li><span id=\"{entry['id']}\">{entry['name']}</span>"
        if 'data' in entry:
            # IF the data entry exists, that means we'll have an inner loop.
            # and it needs to be wrapped in in a li/ol combo
            output += f"<ol>"
            output += parse_output(entry)
            output += f"</ol>"
        output += "</li>"
    return output

if __name__ == '__main__':
    args = docopt(__doc__)

    log_level = 'INFO'
    if args['--debug']:
        log_level = 'DEBUG'
    logging.basicConfig(level=log_level)
    main(args)