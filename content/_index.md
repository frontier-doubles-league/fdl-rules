---
title: Home
menu: sidebar
weight: 1
---

## Frontier Doubles League

This is a North American 2v2s league that is available for 14+ years old PC/PS4/XBOX Rocket League players that are looking for fun, open, non-toxic competitive scene.

The Staff of FDL presents:

### The League Rulebooks

- [Community/League Code of Conduct]({{< relref "page/rulebook/conduct.md" >}})
- [League Rulebook]({{< relref "page/rulebook/league.md" >}})

### Notice from the Congress team

The Frontier Doubles League has the ability to edit these rulebooks when it is deemed necessary. It is the responsibility of the General Managers (GM), Assistant General Managers (AGM), Captains, and Players to understand and abide by this rulebook for league play. If there are any questions please seek help from your team's GM/AGM before directly contacting staff.

This rulebook is solely used for league play, if you have questions unrelated to league play check out the other rulebooks before contacting staff.

Rules will not be edited after roster lock in a given season.

Ideas and suggestions will be taken into account for future seasons of FDL to make the quality of league play better for all players. Before making any suggestions, read through the rulebooks to ensure that your ideas are not already listed.

If there is an error, concerns, or areas that need extra clarification please do not hesitate to inform staff.

Sincerely,

FDL Congress

### Community Information

Come join our discord today! [discord server](https://discord.gg/aJxmpjc)
