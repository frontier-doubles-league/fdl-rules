# FDL Rules

What is this script?

This script will generate the rules based on a supplied json object.

## Development

To run this on a local linux installation, perform the following steps to setup the virtual environment and enable it.

```
# Activate a new environment
python3 -m venv venv

# engage the new venv
source venv/bin/activate

# install packages
pip3 install -r requirements.txt
```

Set the required Environment variables, then you can startup the bot.

```
python3 generator.py
```