---
title: "League Rules"
draft: false
date: 2022-02-04T15:31:30-05:00
menu:
  sidebar:
    parent: Rulebooks
slug: rulebooks/league
weight: 15
autonumbering: true
---

## League Rules

1. Player Applications
    * Players that apply who are under 16 years of age are not eligible for league play, and their application will be refused.
    * The FDL reserves the right to refuse any application.
    * Applicants accepted into the FDL will join the league as free agents.
2. Free Agents
    * Free agents joining the league during the preseason may be selected by one of the team captains during the preseason signing period.
    * All players (including captains, players on teams, and free agents) will be assigned a salary based on their highest competitive doubles and standard Match Making Rank (MMR). If peak doubles MMR is higher, only that MMR is used. If standard is a higher peak, the average of the peak doubles and standard MMR is used. Salaries are calculated by rounding down said MMR to the nearest 10 and dividing by 100.
        * For example, a 1029 MMR would yield a salary of 10.2.
    * Players are placed in a league based on their salary.
        * Players with a salary of 9.9 or lower will be placed in Origin League.
        * Players with a salary of 10.0 to 11.9 will be placed in Adept League.
        * Players with a salary of 12.0 to 13.9 will be placed in Expert League.
        * Players with a salary of 14.0 to 15.9 will be placed in Star League.
        * Players with a salary of 16.0 to 17.9 will be placed in Elite League.
3. Teams
    * All teams may have up to three (3) starters and one (1) reserve, with the exception of Origin League which will be two (2) starters and one (1) reserve.
    * Team captains may trade players with different teams or with free agents through Week 6. Any trades must be authorized by the FDL.
        * Team captains must provide a reason for trading players to the FDL.
        * Any trades that may result in any team’s salary rising over the salary cap are not allowed.
        * Teams already over the salary cap at the time of the trade may not replace the player being traded with a player of an equal or higher salary.
        * In any trading scenario, the FDL, the players being traded, and the captains involved must be informed before any trade takes place.
        * The FDL reserves the right to reject any trades.
    * Every player must play in a minimum of six (6) series during the Regular Season.
        * If a player joins a team at any point in the Regular Season, said player will take on the number of series of the player that was replaced.
        * If two (2) or three (3) players are replaced on a team, then the series of the replaced players will be taken on in the order of their respective salaries, with the higher-salary player taking on the number of series for the higher-salary player being replaced, and the lower-salary player taking on the number of series for the lower-salary player being replaced.
    * At the start of week 7 till the completion of the postseason, no team will be allowed to drop or add players to their roster. This is called the ‘Roster Lock’ period.
    * Each team is allowed 3 changes to the starting roster until Roster Lock.
    * Conference and divisional alignment may be seen at the bottom of this rulebook in Tables 1-5.
4. Reserves
    * One (1) reserve spot is allowed per team. Free agents may be offered reserve spots in the same way that starter spots are offered.
    * Reserve spots may be offered once all the teams in the respective league fill their starting rosters
    * Reserves may substitute for starters up to two times over the course of the season.
        * The exception being Origin League which has unlimited reserve usage.
    * Reserves may substitute for starters at or above the reserve player’s salary.
5. Ranking Out
    * If a player's salary goes over their respective league’s salary cap they will have the option to "rank-out" to the next-level league.
        * The FDL reserves the right to check players' ranks throughout the course of a season, as well as take any action necessary to preserve competitive integrity.
        * Players can rank out of their respective league through Week 6. At the start of Week 7, salary updates are paused and players will remain on their teams for the reminder of the season.
        * Players who are within the top 0.3 salary in their respective league may be “pulled up” to the next league by being signed by a team in the league above them, where they will be placed at the lowest salary in that upper league.
            * This may only happen once per player per season.
6. League Format
    * The FDL has a Preseason, a Regular Season, and a Postseason.
        * The Preseason will occur over two (2) weeks, followed directly by the Regular Season.
        * The Regular Season will occur over ten (10) weeks.
        * Teams will play one match each week in the Regular Season.
    * The FDL is divided into five (5) leagues: Elite League, Star League, Expert League, Adept League, Origin League
        * The number of teams in these leagues is dependent on how many players are signed up to play within the given league salary.
    * Teams will play ten (10) best-of-five (bo5) matches during the Regular Season.
        * Leagues with 8 or 16 teams will play the teams in their conference twice and the outside conference once.
        * Teams with 6 or 12 teams will play all teams in a double round robin.
    * Teams are ranked based on the number of best-of-five (bo5) series they have won.
        * In the event that two (2) or more teams are tied in terms of series won, tiebreakers are as follows:
        * First, head-to-head game win-loss differential between all tied teams.
        * Second, the game win-loss differential.
        * Third, if there is still a tie, a best of five (Bo5) match will be used to determine the tiebreaker.
7. Post-season
    * The Post-season will consist of a single elimination bracket.
        * Origin, Adept and Elite Leagues will host six (6) teams.
        * Star and Expert Leagues will host eight (8) teams.
    * For each division, the teams reaching the postseason will compete in a single-elimination bracket.
        * The first and second round of the playoff bracket will consist of 3 best of 3s
        * Each best of 3 will use a different lineup chosen by each team until all available lineups have been used.
        * The championship round will consist of 3 best of 5s
8. Match Structure
    * All games within FDL matches will be in doubles (2 vs. 2) format.
    * All Regular Season matches will be best-of-five (5) games, with each game lasting five (5) minutes and any overtime that occurs.
    * If a player loses connection during the first minute of a match and before a goal has been scored, then said match must be restarted.
    * Players may request one (1) total server reset per series in-between games.
    * Matches must be played according to the franchises home field. See website for which team is home and away.
    * Team colors are not required for non-streamed FDL matches.
    * In-game names must match the player’s Discord username while in official matches.
    * At least one player on each team must save the match replay after every game has concluded.
    * Default servers are US East. US West may be selected if agreed upon by both teams.
9. Match Scheduling
    * All Regular Season matches are to be scheduled by mutual agreement between both captains of the teams participating in each match.
        * Matches must be scheduled on Thursday through Sundays.
        * Team captains must provide the FDL with the agreed date, and time by Wednesday (11:59pm EST) for the coming week’s matches.
        * Matches may be scheduled the Monday after the current week if scheduling complications arise. This is the latest a match may be scheduled. League Operations approval is needed.
    * Any match not scheduled within the allocated time period or played at the scheduled match time may result in penalties for the teams and players involved. Any such penalties are at the discretion of the FDL.
    * If a team forfeits as a result of a no show more than two (2) times, the players will be ineligible for the rest of the season.
10. Player Responsibilities
    * Players must remain in regular communication with their teammates.
    * Team captains must maintain regular communication with opponents regarding scheduling.
    * Players must respond in a timely manner to any requests or communications from the FDL, regardless of whether or not these are directed towards specific players, their teams, or the entire league.
    * Players must inform their team captains if they are likely to be unavailable to contact.
    * Team captains must provide replays of each game their team has played.
    * Players must show up for their scheduled matches on-time.
11. End-of-Season
    * Once the Regular Season ends, all players wishing to compete in the next season of FDL need to fill out an intent form application located on the website.
    * Any captains or players that do not respond by the stated deadline will be removed from their current team and placed in the free agent pool.
    * Anyone eligible to play in the coming season can apply to be a captain.
        * The FDL reserves the right to deny anyone from becoming or remaining a captain.
12. Preseason Signing Period
    * A signing period will occur before the start of each Regular Season.
        * During the signing period, free agents may be offered any open spots on teams. Free agents may accept or deny these offers at their discretion.
        * The FDL will finalize any accepted offers. If any offers accepted would push a team’s salary over the salary cap, then they will be denied.
    * The FDL will provide set dates and times for the signing period to occur.
    * Prior to the signing period, all team captains will be provided with a list of all free agents available, time zones, and salaries.
    * Team captains must fill any open spots on their teams by the end of the signing period.
13. FDL Responsibilities
    * The FDL is committed to providing the following:
        * An organized, professional league where players are treated fairly.
        * Weekly updated team and player statistics, results, and standings delivered in a timely fashion.
        * A dedicated Discord channel for all members of the FDL community to use.
        * Regular streaming and casting of series when possible.
        * A Congress of four (4) members who make collective decisions on league matters. A maximum of five (5) people may be on Congress at a single time.
        * Improvement and updating of rules when necessary. Rules may be changed at any time, and at the FDL’s discretion.

## Index

Table 1 - Conference Layout for Origin League
{{<table "table table-striped table-bordered">}}
| Stealth Conference | Courage Conference |
|----------|----------|
| Eagles   | Dragons  |
| Hornets  | Griffins |
| Owls     | Huskies  |
| Seagulls | Vipers   |
{{</table>}}

Table 2 - Conference Layout for Adept League
{{<table "table table-striped table-bordered">}}
| Stealth Conference | Courage Conference |
|----------|----------|
| Coyotes  | Dragons  |
| Eagles   | Griffins |
| Gators   | Rams     |
| Owls     | Vipers   |
{{</table>}}

Table 3 - Conference Layout for Expert League
{{<table "table table-striped table-bordered">}}
| Stealth Conference | Courage Conference |
|----------|----------|
| Coyotes  | Colts    |
| Eagles   | Huskies  |
| Hornets  | Hydras   |
| Gators   | Griffins |
| Penguins | Monkeys  |
| Seagulls | Vipers   |
{{</table>}}

Table 4 - Conference Layout for Star League
{{<table "table table-striped table-bordered">}}
| Stealth Conference | Courage Conference |
|----------|----------|
| Coyotes  | Colts    |
| Gators   | Dragons  |
| Hornets  | Hydras   |
| Lobsters | Griffins |
| Penguins | Monkeys  |
| Seagulls | Rams     |
{{</table>}}

Table 5 - Conference Layout for Elite League
{{<table "table table-striped table-bordered">}}
| Stealth Conference | Courage Conference |
|----------|----------|
| Lobsters | Dragons  |
| Gators   | Hydras   |
| Owls     | Monkeys  |
| Seagulls | Rams     |
{{</table>}}