---
comments: false
menu: sidebar
weight: 45
title: Changelog
disablerss: true
---

# Changelog
All notable changes to the rules will be posted here.
## [Unreleased]

## [6.0.0] - 2022-07-24
<i>Season 6 Adjustments.</i>

### Added
- Add wording for 3 roster changes allowed per-season.
- Add quantity of teams from each league that will be included in the postseason.
- Add forfeit wording to add negative impact in the result of a no-show.

### Changed
- Change wording around Salary calculations for rounding down policy.
- Change Salary listing per-league.
- Change Conference/Division Alignment.
- Change reserve usage to allow unlimited uses in origin league.
- Change tie-breaker format, removes seasonal goal differential, adds a Best of Five as a final tie-breaker.


## [5.2.0] - 2022-03-19
<i>The Big One for Season 5.</i>

### Added
- Added "Index" for Tables with conference and divisional alignment.
- Added 6.1.1, Pre-season language establishing pre-season play.
- Added 6.2.1, wording to set franchise participation in a given league.
- Added 8.4, explicit wording to allow only one server reset per series.
### Changed
- Changed 3.1, to reflect 1 reserve for non-elite league-leagues.
- Changed 11.1, with wording for intent to play in the next season.
- Changed 5.1, rank-out language to make it compulsary while making clarity around what salary ranges can be "pulled up" to the next league and how.
- Changed 7, the language around post-season structure and play format.
- Changed 4.1, reserve usage from 3 to 2 to reflect the 1 reserve rule change.
- Changed 3.3, the wording from maximum of 7 series to minumum of 6 series for compliance clarity.
### Removed
- Removed Salary cap information as it's freely available on the website.
- Removed postseason wording about usage no usage restrictions.
- Removed wording that explicitly allowed reserves to be signed as a starter for other teams.
- Removed allowed field of play as teams have home fields now.
## [5.1.0] - 2022-02-08
### Added
- Added complete port of Season 4 Rulebook with Code of Conduct section ommitted.
## [5.0.1] - 2022-02-06
### Changed
- Changed the language of rule 13 in the Code of Conduct to specify names in addition to pictures.
## [5.0.1] - 2022-02-04
### Added
- Added Rule 13 to the [Community code of conduct]({{< relref "page/rulebook/conduct.md" >}}) which bans NSFW profile pictures.

## [5.0.0] - 2022-02-04
### Added
- Added version 5.0.0 of the [Community code of conduct]({{< relref "page/rulebook/conduct.md" >}})

### Changed
- Updated the rules website to v5.0.0 which includes a shift to automatic website deployment and changelog management.